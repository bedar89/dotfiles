# -*-mode:sh-*- vim:ft=shell-script

# ~/.zshrc
# =============================================================================
# Executed by Z shell for interactive shells.
#
# See http://zsh.sourceforge.net/

# shellcheck shell=bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Hide default user from local prompt.
export DEFAULT_USER="${USER}"

# Disable autosuggestion for large buffers.
export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE="20"

# Enable aynchronous mode.
export ZSH_AUTOSUGGEST_USE_ASYNC=true

# Set path to your oh-my-zsh installation.
export ZSH="${HOME}/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"
ZSH_THEME="powerlevel9k/powerlevel9k"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd"

setopt histignorespace  # prepending spaces are security breaches
setopt rm_star_silent  # do not query the user if e.g. rm *

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    # asdf                    # Integrates with ASDF extendable version manager
    colored-man-pages       # Adds colors to man pages
    common-aliases          # Aliases for many commonly used commands
    cp                      # Secure `cpv` function that uses `rsync`
    docker                  # Aliases for [Docker](https://www.docker.com/)
    docker-compose          # Aliases for [Docker-Compose](https://docs.docker.com/compose/)
    dotenv                  # Load project ENV variables from folder `.env` files
    extract                 # Multi-format decompression function `x`
    history                 # Aliases for `history` command
{{- if eq .chezmoi.os "darwin" }}
	osx                 # Utilities for macOS
{{- end -}}
	per-directory-history   # History for every directory
    pj                      # Project Jump folder shortcuts
    rsync                   # Aliases for `rsync` command
    sublime                 # Aliases for [Sublime Text](https://www.sublimetext.com/)
    systemadmin             # Utilities for SysAdmins.
	vi-mode					# use vi in the terminal (e.g. <a-j> for last command)...
    z                       # Tracks your most used directories.
    zsh-autosuggestions     # Command suggestions based on history and completions (https://github.com/zsh-users/zsh-autosuggestions)
    zsh-syntax-highlighting # Fish shell like syntax highlighting (https://github.com/zsh-users/zsh-syntax-highlighting)
)


# Paths
# -----------------------------------------------------------------------------

fpath=($fpath $HOME/.local/share/zsh/vendor-completions)


# Includes
# -----------------------------------------------------------------------------

# Load theme Powerlevel10K
if test "${ZSH_THEME#*powerlevel10k}" != "$ZSH_THEME"; then
    [ -f "$HOME"/.p10k.zsh ] && \. "$HOME"/.p10k.zsh
fi

# Load Oh My Zsh
. "$ZSH"/oh-my-zsh.sh

# Load cross-compatible Bash functions declarations from separate file.
if [ -f "$HOME"/.bash_functions ]; then
    \. "$HOME"/.bash_functions
fi

# Load cross-compatible Bash alias definitions from separate file.
if [ -f "$HOME"/.bash_aliases ]; then
    \. "$HOME"/.bash_aliases
fi

# Load cross-compatible Bash exports, flags and setings from separate file.
if [ -f "$HOME"/.bash_settings ]; then
    \. "$HOME"/.bash_settings
fi

# Load custom code. Intended for settings specific to the user. This is to be added to dotfiles repo (private branch).
if [ -f "$HOME"/.bash_extras ]; then
    \. "$HOME"/.bash_extras
fi

# Load custom code. Intended for settings specific to the machine. Not to be added to dotfiles repo.
if [ -f "$HOME"/.bash_local ]; then
    \. "$HOME"/.bash_local
fi
