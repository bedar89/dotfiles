augroup CBuild
  autocmd!
  autocmd filetype c   nnoremap <buffer> <leader>cc :!gcc -o %:p:r %<cr>
  autocmd filetype c   nnoremap <buffer> <leader>cr :!gcc -o %:p:r %<cr>:!%:p:r<cr>
  autocmd filetype cpp nnoremap <buffer> <leader>cc :!g++ -o %:p:r %<cr>
  autocmd filetype cpp nnoremap <buffer> <leader>cr :!g++ -o %:p:r %<cr>:!%:p:r<cr>
augroup END