<h1 align="center">
    <a name="top" title="dotfiles">~/.&nbsp;📂</a><br/>Cross-platform, cross-shell dotfiles<br/> <sup><sub>powered by  <a href="https://www.chezmoi.io/">chezmoi</a> 🏠</sub></sup>
</h1>

One-command setup of new machines (linux, windows, mac) including setting configurations (dotfiles) and installing binaries, securely. Easy to customize to your needs (private branch) and designed to be generic and for multi-user collaboration (master).

Content:

[[_TOC_]]

# Project goals ⚽

- Unified set of aliases and commands.
- Familiar feel and creature comforts across environments.
- Cross-platform file management toolset.
- A clean, non-distracting interface! 💃
- Generic configuration on master for sharing, personal on branch called "personal".
- Include secrets in keepassxc.

## Concepts

### SICK Navigation

#### Introduction

SICK navigation refers to the practice of using a set of Standardized, Intuitive, and Consistent Keystrokes (SICK) across different software programs and contexts to navigate more efficiently. This can help increase productivity, reduce the learning curve when switching between applications, and make it easier for users to remember and use keyboard shortcuts.

#### Customizing Keyboard Shortcuts

To customize your keyboard shortcuts, you can use the built-in customization options in your software program or use a third-party tool or plugin. Here are a few examples of how to customize keyboard shortcuts:

- Define a new shortcut for a frequently used command, such as "Save" or "Copy".
- Change an existing shortcut to a more memorable or intuitive key combination.
- Use the same shortcuts across different software programs and contexts, such as using `Ctrl+C` to copy text in both a text editor and a web browser.

#### Using SICK Navigation

To make the most of SICK navigation, it's important to develop a set of shortcuts that are easy to remember and use, and to use them across different software programs and contexts. Here are a few tips for using SICK navigation effectively:

- Use mnemonic devices to help remember keyboard shortcuts. For example, you might use `Ctrl+S` to remember "Save" or `Ctrl+C` to remember "Copy".
- Customize keyboard shortcuts to match your preferences or to match the shortcuts of other programs you're familiar with.
- Use a tool or plugin that allows you to define system-wide keyboard shortcuts, so you can use the same shortcuts across different applications and contexts.

#### TODO/Ideas

- Get a grip about frequency of tasks.
    - → This way, we can choose hierarchy e.g. `<leader>f` for file `<leader>b` for buffer.
    - → Emperical decisions possible
    - Problem is: Try to make frequency a function of variables that are invariant across professions and situation (professional/private)
- Polymorphisms allow automizations?
- Layouts can be defined that ease the implementation of SICK navigation across apps

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

# Setup

## Prerequisits

- [git](https://git-scm.com/) version-control system
- [chezmoi](https://www.chezmoi.io/install/) dotfiles manager

Typically you only have to install chezmoi. We propose to install with a pre-built package to *$HOME/.local/bin*.
To do that,

1. [Download the prebuild binaries](https://www.chezmoi.io/install/#download-a-pre-built-binary).
    <details>
    <summary>
    Symlink the Downloads folder if you use wsl (delete the Download folder if you have one)
    </summary>

        WINUSER=$(powershell.exe -c "Write-Host -NoNewLine ([Environment]::UserName)" 2>/dev/null)
        ln -s /mnt/c/Users/$WINUSER/Downloads

    </details>
1. Extract the archive.

        mkdir $HOME/Downloads/chezmoi
        tar -xf $HOME/Downloads/chezmoi*.tar.gz -C $HOME/Downloads/chezmoi

1. Put the executable to `~/.local/bin`.
    The completion folder should mege with `~/.local/share/bash-completion/completions/`.

        mkdir -p ~/.local/bin
        cp ~/Downloads/chezmoi/chezmoi ~/.local/bin/
        mkdir -p ~/.local/share/fish/vendor_completions.d
        cp ~/Downloads/chezmoi/completions/chezmoi.fish ~/.local/share/fish/vendor_completions.d
        mkdir -p ~/.local/share/bash-completion/completions/
        cp ~/Downloads/chezmoi/completions/chezmoi-completion.bash ~/.local/share/bash-completion/completions/
        mkdir -p ~/.local/share/zsh/vendor-completions/
        cp ~/Downloads/chezmoi/completions/chezmoi.zsh ~/.local/share/zsh/vendor-completions/

## Installation 🚀

```bash
chezmoi init https://gitlab.com/dboe/dotfiles.git
chezmoi apply
```

<details>
<summary>
Data from last initialization?
</summary>
Chezmoi does (to my knowledge not have a command to show the config data (not `chezmoi data`).
However you can see your data at

    cat ~/.config/chezmoi/chezmoi.toml

</details>

<details>
<summary>
Encounter problems?
</summary>
Please open an [issue](https://gitlab.com/dboe/dotfiles/issues) or [pull request](https://gitlab.com/dboe/dotfiles/-/merge_requests) if you encounter problems.
</details>

## Security

`chezmoi init` asks for your keepass file because keepassxc (automatically installed by `chemzoi apply`) is used to safely handle your security relevant information.
For all parts of this to work flawlessly your keepass database keys have to have the following signature:


Information | Entry name | Further
---------|----------|---------
 ssh private/public key pair | 'ssh key' | Attach the private key file with the name id_ed25519 (requires 'keepassxc>=2.7.0'). Your private key is then put to '\<HOME\>/.ssh/id_ed25519'


# Supported toolset 🛠️

Use either one or many of these, the config files will be in place and ready to provide a familiar interface.

## 🐚 Shell

- [Bash](https://www.gnu.org/software/bash/) <sub><sup><b title="Linux">🐧</b></sup></sub><b title="macOS"></b>: [`~/.bashrc`](./dot_bashrc) _<sup>enhanced with [**Bash-It**](https://github.com/Bash-it/bash-it)!</sup>_
- [PowerShell 6.X](https://github.com/PowerShell/PowerShell) <sub><sup><b title="Linux">🐧</b></sup></sub><b title="macOS"></b><b title="Windows">⊞</b>: [`~/.config/powershell/`](./dot_config/powershell/) _<sup>enhanced with [**Oh-My-Posh**](https://github.com/JanDeDobbeleer/oh-my-posh), [**Terminal Icons**](https://github.com/devblackops/Terminal-Icons), and others!</sup>_
- [Z shell](http://zsh.sourceforge.net/) <sub><sup><b title="Linux">🐧</b></sup></sub><b title="macOS"></b>: [`~/.zshrc`](./dot_zshrc) _<sup>enhanced with [**Oh-My-Zsh**](https://ohmyz.sh/), [**Powerlevel10K**](https://github.com/romkatv/powerlevel10k), and others!</sup>_
zsh

## 💻 Terminals

- [Hyper](https://hyper.is/) <sub><sup><b title="Linux">🐧</b></sup></sub><b title="macOS"></b><b title="Windows">⊞</b>: [`~/.hyper.js`](./dot_hyper.js.tmpl)
- [iTerm2](https://iterm2.com/) <b title="macOS"></b>: [`~/.config/iterm/`](./dot_config/iterm)
- [macOS Terminal](https://support.apple.com/en-ca/guide/terminal/welcome/mac) <b title="macOS"></b>
- [Windows Terminal](https://www.microsoft.com/en-us/p/windows-terminal-preview/9n0dx20hk701) <b title="Windows">⊞</b>: [`~/.config/windows_terminal/`](./dot_config/windows_terminal)

## Cross-shell compatibility matrix 🏁

These are unified CLI commands available amongst different shells on all platforms. While some of their outputs may differ in style between different environments, their usage and behaviours remain universal.

Additional aliases are provided by [Bash-It](https://github.com/Bash-it/bash-it/tree/master/aliases/available), [Oh-My-Zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Cheatsheet) and [Powershell](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_aliases), and are listed by using the command `alias`.

System-specific aliases are marked with <b title="macOS"></b>, <b title="Windows">⊞</b>, or <sub><sup><b title="Linux">🐧</b></sup></sub>.

### 🧭 Easier navigation

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅ | `cd ~`      | Navigates to user home directory. |
| ✅   | ✅         | ✅ | `cd -`    | Navigates to last used directory. (from at least powershell version 6.2) |
| ✅   | ✅         | ✅ | `..`     | Navigates up a directory. |
| ✅   | ✅         | ✅ | `...`    | Navigates up two directories. |
| ✅   | ✅         | ✅ | `....`   | Navigates up three directories. |
| ✅   | ✅         | ✅ | `.....`  | Navigates up four directories. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🗂️ Directory browsing

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `l`     | Lists visible files in long format. |
| ✅   | ✅         | ✅  | `ll`    | Lists all files in long format, excluding `.` and `..`. |
| ✅   | ✅         | ✅  | `lsd`   | Lists only directories in long format. |
| ✅   | ✅         | ✅  | `lsh`   | Lists only hidden files in long format. |
| ✅   | 🛠️         | ✅  | `resolve` | Resolve paths (relative and symlinks) to the absolute path |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🗄️ File management

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `cpv`   | Copies a file securely (rsync). |
| ✅   | ✅         | ✅  | `fd`    | Finds directories. |
| ✅   | ✅         | ✅  | `ff`    | Finds files. |
| ❌   | ✅         | ❌  | `mirror` | Mirrors directories. |
| ✅   | ✅         | ✅  | `rg`    | Searches recursively with [ripgrep](https://github.com/BurntSushi/ripgrep). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 💡 General aliases

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `alias` | Lists aliases. |
| ✅   | ✅         | ✅  | `c`     | Clears the console screen. |
| ✅   | ❌         | ✅  | `extract`<br>`x` | Extracts common file formats.<br>_Usage: `extract solarized.zip`_ |
| ✅   | ✅         | ✅  | `h`     | Displays/Searches global history.<br>_Usage: `h`_<br>_Usage: `h cd`_ |
| ✅   | ✅         | ⚠️  | `hs`    | Displays/Searches session history.<br>_Usage: `hs`_<br>_Usage: `hs cd`_ |
| ✅   | ✅         | ✅  | `mkcd`<br>`take` | Creates directory and change to it.<br>_Usage: `mkcd foldername`_ |
| ✅   | ❌         | ✅  | `reload` | Reloads the shell. |
| ✅   | ✅         | ✅  | `repeat`<br>`r` | Repeats a command `x` times.<br>_Usage: `repeat 5 echo hello`_. |
| ✅   | ❌         | ✅  | `resource` | Reloads configuration. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🕙 Time

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `now`<br>`unow` | Gets local/UTC date and time in [ISO 8601](https://xkcd.com/1179/) format `YYYY-MM-DDThh:mm:ss`. |
| ✅   | ✅         | ✅  | `nowdate`<br>`unowdate` | Gets local/UTC date in `YYYY-MM-DD` format. |
| ✅   | ✅         | ✅  | `nowtime`<br>`unowtime` | Gets local/UTC time in `hh:mm:ss` format. |
| ✅   | ✅         | ✅  | `timestamp` | Gets Unix time stamp. |
| ✅   | ✅         | ✅  | `week`  | Gets week number in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Week_dates) format `YYYY-Www`. |
| ✅   | ✅         | ✅  | `weekday` | Gets weekday number. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🌐 Networking

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `fastping` | Pings hostname(s) 30 times in quick succession. |
| ✅   | ✅         | ✅  | `flushdns` | Flushes the DNS cache. |
| ✅   | ✅         | ✅  | `ips`   | Gets all IP addresses. |
| ✅   | ✅         | ✅  | `localip` | Gets local IP address. |
| ✅   | ✅         | ✅  | `publicip` | Gets external IP address. |
| ✅   | ✅         | ✅  | `GET`<br>`HEAD`<br>`POST`<br>`PUT`<br>`DELETE`<br>`TRACE`<br>`OPTIONS` | Sends HTTP requests.<br>_Usage: `GET https://example.com` |_

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### ⚡ Power management

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `hibernate` | Hibernates the system. |
| ✅   | ✅         | ✅  | `lock`  | Locks the session. |
| ✅   | ✅         | ✅  | `poweroff` | Shuts down the system. |
| ✅   | ✅         | ✅  | `reboot` | Restarts the system. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🤓 Sysadmin

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `mnt`   | Lists drive mounts. |
| ✅   | ✅         | ✅  | `path`  | Prints each `$PATH` entry on a separate line. |
| ✅   | ✅         | ✅  | `sysinfo` | Displays information about the system.<br><strong><sup>Uses either [Winfetch](https://github.com/lptstr/winfetch), [Neofetch](https://github.com/dylanaraps/neofetch), or [Screenfetch](https://github.com/KittyKatt/screenFetch).</sup></strong> |
| ✅   | ✅         | ✅  | `top`   | Monitors processes and system resources.<br><strong><sup>Uses either [atop](https://linux.die.net/man/1/atop), [htop](https://hisham.hm/htop/), [ntop](https://github.com/Nuke928/NTop) <b title="windows">⊞</b>, or native.</sup></strong> |
| ✅   | ✅         | ✅  | `update` | Keeps all apps and packages up to date. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🖥️ Applications

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `browse` | Opens file/URL in default browser.<br>_Usage: `browse https://example.com`_ |
| ✅   | ✅         | ✅  | `chrome` | Opens file/URL in [Chrome](https://www.google.com/chrome/). |
| ✅   | ✅         | ✅  | `edge` | Opens file/URL in [Microsoft Edge](https://www.microsoft.com/en-us/edge). |
| ✅   | ✅         | ✅  | `firefox` | Opens file/URL in [Firefox](https://www.mozilla.org/en-CA/firefox/). |
| ❔   | ✅         | ❔  | `iexplore` | Opens file/URL in [Internet Explorer](https://www.microsoft.com/ie). <b title="Windows">⊞</b> |
| ✅   | ✅         | ✅  | `opera` | Opens file/URL in [Opera](https://www.opera.com/). |
| ✅   | ✅         | ✅  | `safari` | Opens file/URL in [Safari](https://www.apple.com/ca/safari/). <b title="macOS"></b> |
| ✅   | ✅         | ✅  | `ss`    | Enters the [Starship 🚀](https://starship.rs) cross-shell prompt. |
| ✅   | ✅         | ✅  | `subl`<br>`st`  | Opens in [Sublime Text](https://www.sublimetext.com/). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 👩‍💻 Development

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `dk`    | 🐳 Alias for [`docker`](https://www.docker.com/). |
| ✅   | ✅         | ✅  | `dco`   | 🐳 Alias for [`docker-compose`](https://docs.docker.com/compose/). |
| ✅   | ✅         | ✅  | `g`     | :octocat: Alias for [`git`](https://git-scm.com/). |
| ✅   | ✅         | ✅  | `va`    | 🐍 Activates Python [virtual environment `venv`](https://docs.python.org/3/tutorial/venv.html). |
| ✅   | ✅         | ✅  | `ve`    | 🐍 Creates Python [virtual environment `venv`](https://docs.python.org/3/tutorial/venv.html). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

###  macOS

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `hidedesktop`<br>`showdesktop` | Toggles display of desktop icons. |
| ✅   | ✅         | ✅  | `hidefiles`<br>`showfiles` | Toggles hidden files display in [Finder](https://support.apple.com/en-ca/HT201732). |
| ✅   | ✅         | ✅  | `spotoff`<br>`spoton` | Toggles [Spotlight](https://support.apple.com/en-ca/HT204014). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### ⊞ Windows

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ❔   | ✅         | ❔  | `hidefiles`<br>`showfiles` | Toggles hidden files display in [File Explorer](https://support.microsoft.com/en-ca/help/4026617/windows-10-windows-explorer-has-a-new-name). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 📁 Common paths

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `dls`   | Navigates to `~/Downloads`. |
| ✅   | ✅         | ✅  | `docs`  | Navigates to `~/Documents`. |
| ✅   | ✅         | ✅  | `dt`    | Navigates to `~/Desktop`. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 📁 Configuration paths

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `chezmoiconf` | Navigates to [Chezmoi](https://www.chezmoi.io/)'s local configuration repo. |
| ✅   | ✅         | ✅  | `powershellconf` | Navigates to [Powershell](https://github.com/PowerShell/PowerShell)'s profile location. |
| ✅   | ✅         | ✅  | `sublimeconf` | Navigates to [Sublime Text](https://www.sublimetext.com/)'s local configuration repo. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 📁 Custom paths

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `archives` | Navigates to `~/Archives`. |
| ✅   | ✅         | ✅  | `repos` | Navigates to `~/Code`. |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

### 🌱 Varia

| Bash | PowerShell | Zsh | Command | Description |
|:----:|:----------:|:---:|---------|-------------|
| ✅   | ✅         | ✅  | `cb`    | 📋 Copies contents to the clipboard. |
| ✅   | ✅         | ✅  | `cbpaste` | 📋 Pastes the contents of the clipboard. |
| ✅   | ✅         | ✅  | `md5sum` | #️⃣ Calculates MD5 hashes. |
| ✅   | ✅         | ✅  | `sha1sum`  | #️⃣ Calculates SHA1 hashes. |
| ✅   | ✅         | ✅  | `sha256sum` | #️⃣ Calculates SHA256 hashes. |
| ✅   | ✅         | ✅  | `forecast` | 🌤️ Displays [detailed weather and forecast](https://wttr.in/?n). |
| ✅   | ✅         | ✅  | `weather` | 🌤️ Displays [current weather](https://wttr.in/?format=%l:+(%C)+%c++%t+[%h,+%w]). |

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

## 📦 Package managers

- [Homebrew](https://brew.sh/) <b title="macOS"></b>: [`~/.Brewfile`](./dot_Brewfile)
- [Scoop](https://scoop.sh/) <b title="Windows">⊞</b>

## 💾 Universal apps <sup><sub><b title="Linux">🐧</b></sub></sup><b title="macOS"></b><b title="Windows">⊞</b>

- [chezmoi](https://www.chezmoi.io/) dotfiles manager: [`~/.chezmoi.toml`](./.chezmoi.toml.tmpl)
- [cURL](https://curl.haxx.se/) data transfer tool: [`~/.curlrc`](./dot_curlrc)
- [Git :octocat:](https://git-scm.com/) version-control system: [`~/.gitconfig`](./dot_gitconfig.tmpl)
- [GNU Nano 4.x+](https://www.nano-editor.org/) text editor: [`~/.nanorc`](./dot_nanorc) _<sup>enhanced with [**Improved Nano Syntax Highlighting Files**](https://github.com/scopatz/nanorc)!</sup>_
- [GNU Wget](https://www.gnu.org/software/wget/) HTTP/FTP file downloader: [`~/.wgetrc`](./dot_wgetrc)
- [Micro](https://micro-editor.github.io/) text editor: [`~/.config/micro/`](./dot_config/micro)
- [OpenSSH](https://www.openssh.com/) secure networking utilities: [`~/.ssh/config`](./dot_ssh/config.tmpl)
- [Ripgrep](https://github.com/BurntSushi/ripgrep) fast-search tool: [`~/.ripgreprc`](./dot_ripgreprc)
- [SQLite3](https://www.sqlite.org/cli.html) database client: [`~/.sqliterc`](./dot_sqliterc)
- [Starship 🚀](https://starship.rs) cross-shell prompt: [`~/.config/starship.toml`](./dot_config/starship.toml)
- [tmux](https://github.com/tmux/tmux/wiki) terminal multiplexer: [`~/.tmux.conf.local`](./dot_tmux.conf.local) _<sup>enhanced with [**Oh-My-Tmux**](https://github.com/gpakosz/.tmux)!</sup>_
- [Vim](https://www.vim.org/) text editor: [`~/.vimrc`](./dot_vimrc) _</sup>_

You can easily modify what is to be installed by adding/removing packages from home/.chezmoidata.yaml -> packages / headful_packages.

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

## Safety

Personal secrets are stored in [Keepass XC](https://keepassxc.org).
Required entries:

    - TODO

<p align="right"><a href="#top" title="Back to top">🔝</a></p>

# Development

## Template debugging

For debugging templates, you can run 
```bash
chezmoi execute-template "$(cat <path/to/my/template/relative/to/chezmoi-home>.toml.tmpl)
```
in bash or in the powershell
```powershell
type home\.chezmoi.toml.tmpl | chezmoi execute-template --init
```

# Inspirations

A lot of the inspriation or source code is assembled from the following projects:

- <https://github.com/twpayne/dotfiles>
- <https://git.vulpecula.fr/niko/dotfiles>
- <https://github.com/renemarc/dotfiles>
- <https://github.com/joelazar/dotfiles>

<p align="right"><a href="#top" title="Back to top">🔝</a></p>
